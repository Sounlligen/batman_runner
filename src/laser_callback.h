#ifndef LASER_CALLBACK_H
#define LASER_CALLBACK_H

#include <ros/ros.h>
#include <math.h>
#include <sensor_msgs/LaserScan.h>

extern float angular_laser;
extern float linear_laser;

void laser_callback (const sensor_msgs::LaserScanConstPtr &message);

#endif // LASER_CALLBACK_H
