#include "joy_callback.h"

float linear_joy;
float angular_joy;
bool control_joy;

void joyCallback(const sensor_msgs::JoyConstPtr &joy)
{
    linear_joy = 0;
    angular_joy = 0;

    if (joy->buttons[0] == 1)
	control_joy = 1;
    if (joy->buttons[1] == 1)
	control_joy = 0;

    if (joy->axes.at(4)<-0.2 || joy->axes.at(4)>0.2)                  //Dead zone for driving forward
        linear_joy = (float)joy->axes.at(4);

    if (joy->axes.at(3)<-0.2 || joy->axes.at(3)>0.2)                  //Dead zone for touring around
        angular_joy = (float)joy->axes.at(3);
}
