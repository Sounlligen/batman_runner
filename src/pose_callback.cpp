#include "pose_callback.h"

float angular_pose;
float linear_pose;

void pose_callback (const nav_msgs::OdometryConstPtr &position)
{
angular_pose = 0;
linear_pose = 0.2;

float x = position->pose.pose.position.x;
float y = position->pose.pose.position.y;
float corr_a = 0;
float corr_b = 0;

printf("x = %.2f\t y = %.2f\n", x, y);

if(abs(x - x_goal) > 0.05 || abs(y - y_goal) > 0.05)
{
    if(x - x_prev < 0)
        corr_a = M_PI;
    if(x_goal - x < 0)
        corr_b = M_PI;
//Contains angle between velocity and x axis
float alpha = atan2(y - y_prev, x - x_prev);
//Contains angle between AB vector and x axis
float beta = atan2(y_goal - y, x_goal - x);

float e = beta - alpha;
if(e == e)
sum += e;

//printf("e = %.2f\t sum = %0.2f\n", e, sum);

//Params for PID
float kp = 5;
float Td = 0;
float Ti = 9999;

angular_pose = kp*(e + 0.1*sum/Ti + Td*(e - e_prev)/0.1);

e_prev = e;
x_prev = x;
y_prev = y;
}
else
{
    angular_pose = 0;
    linear_pose = 0;
    printf("You are the WINNER!\n");
}
}
