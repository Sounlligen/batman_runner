#ifndef POSE_CALLBACK_H
#define POSE_CALLBACK_H

#include <nav_msgs/Odometry.h>
#include <ros/ros.h>

//Contains previous pose
extern float x_prev;
extern float y_prev;

//Contains goal coordinates
extern float x_goal;
extern float y_goal;

//Contains previous error for PID regulator
extern float e_prev;
//Contains previous sum for PID regulator
extern float sum;

extern float angular_pose;
extern float linear_pose;

void pose_callback (const nav_msgs::OdometryConstPtr &position);

#endif // POSE_CALLBACK_H
