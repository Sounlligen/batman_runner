#include <ros/ros.h>
#include <signal.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include "pose_callback.h"
#include "laser_callback.h"
#include "joy_callback.h"

ros::Publisher pub_cmd_vel;
geometry_msgs::Twist msg;

//Variables for previous position
float x_prev;
float y_prev;

//Variables for goal position
float x_goal;
float y_goal;

//Variables for PID
float e_prev;
float sum;

//Variables for controling velocity
float angular;
float linear;

//Function for stopping robot after killing node
void mySigintHandler(int sig)
{
    printf("Caught exit signal\n");
    msg.linear.x = 0; msg.angular.z = 0; pub_cmd_vel.publish(msg);
    ros::shutdown();
}


int main (int argc, char **argv)
{
    ros::init(argc, argv, "batman_runner");

    ros::NodeHandle n;
    ros::Rate rate(10);

    signal(SIGINT, mySigintHandler);


    ros::Subscriber pose_sub = n.subscribe<nav_msgs::Odometry>("/batman/pose", 1, &pose_callback);
    ros::Subscriber laser_sub = n.subscribe<sensor_msgs::LaserScan>("/scan", 1, &laser_callback);
    ros::Subscriber joy_sub = n.subscribe<sensor_msgs::Joy>("/joy", 1, &joyCallback);
    pub_cmd_vel = n.advertise<geometry_msgs::Twist>("/batman/cmd_vel", 1);


    x_prev = 0;
    y_prev = 0;

    x_goal = 0;
    y_goal = -2;

    e_prev = 0;
    sum = 0;

    angular = 0;
    linear = 0.2;

    control_joy = 1;

    while(ros::ok())
    {

        if(control_joy)
        {
            angular = angular_joy;
            linear = linear_joy;
        }
        else
        {
            //If obstacle is close enough, avoid it. In other case go for the goal
            //if(angular_laser < 0.1)
            {
                angular = angular_pose;
                linear = linear_pose;
            }
            /*else
            {
                angular = angular_laser;
                linear = linear_laser;
            }*/
        }


        msg.angular.z = angular;
        msg.linear.x = linear;

        pub_cmd_vel.publish(msg);

        rate.sleep();

        ros::spinOnce();

    }
}
