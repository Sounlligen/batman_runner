#ifndef JOY_CALLBACK_H
#define JOY_CALLBACK_H

#include <sensor_msgs/Joy.h>

extern float linear_joy;
extern float angular_joy;
extern bool control_joy;

void joyCallback(const sensor_msgs::JoyConstPtr &joy);

#endif // JOY_CALLBACK_H
