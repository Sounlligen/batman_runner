#include "laser_callback.h"

float angular_laser;
float linear_laser;

void laser_callback(const sensor_msgs::LaserScanConstPtr &message)
{
angular_laser = 0;
linear_laser = 0.5;
bool is_obstacle = 0;

//Max i corresponds to PI/2 angle
int i_max = (message->angle_max - message->angle_min)/message->angle_increment;

float range_crit = 0.8;
float min_range = message->ranges[0];
//Max angular for alpha = 0
float k = 2/(M_PI);


float sum_left = 0;
float weights_left = 0.01;

for(int i = 0; i <= i_max/2; i++)
{
    if(message->ranges[i] < range_crit)
    {
        if(message->ranges[i] > message->range_min )
        {
            sum_left += i * (range_crit - message->ranges[i]);
            weights_left += range_crit - message->ranges[i];
            is_obstacle = 1;
            if(min_range > message->ranges[i])
                min_range = message->ranges[i];
        }
    }
}


float sum_right = 0;
float weights_right = 0.01;

for(int i = (i_max/2) + 1; i <= i_max; i++)
{
    if(message->ranges[i] < range_crit)
    {
        if(message->ranges[i] > message->range_min)
        {
            sum_right += (i_max - i) * (range_crit - message->ranges[i]);
            weights_right += range_crit - message->ranges[i];
            is_obstacle = 1;
            if(min_range > message->ranges[i])
                min_range = message->ranges[i];
        }
    }
}


float min_range_index_left = sum_left/weights_left;
float min_range_index_right = sum_right/weights_right;

float alpha_left = message->angle_min + min_range_index_left * message->angle_increment;
float alpha_right = message->angle_min + min_range_index_right * message->angle_increment;
//Alpha is the angle between obstacle radius and the middle angle
float alpha = 0;

if(-alpha_left < alpha_right)
	alpha = alpha_left;
else
	alpha = alpha_right;


if(is_obstacle)
{
    if(min_range < 0.4 && abs(alpha) < M_PI/12)
	linear_laser = 0;
    else
	linear_laser = 0.2;

    if(-alpha_left < M_PI/6 && alpha_right < M_PI/6)
	linear_laser = -0.2;

    if(alpha < 0)
    {
        angular_laser = k * (alpha + M_PI/2);
    }
    else
    {
        angular_laser = k * (alpha - M_PI/2);
    }
}
}
